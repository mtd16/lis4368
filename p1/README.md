
# LIS4368

## Michael DeMaria

### Project 1 Requirements:

*Sub-Heading:*

1. Create Server-side validation
2. Create 3 slides to add to the homepage promoting your skills


#### README.md file should include the following items:

* Screenshot of unpopulated tables
* Screenshot of failed validation
* Screenshot of passed validation



#### Assignment Screenshots:

*Screenshot of main/splash home page:*

![Splash page](img/splash.png)

*Screenshot of unpopulated tables:*

![Unpopulated](img/unpopulated.png)

*Screenshot of failed validation*:

![Failed validation](img/failed_validation.png)

*Screenshot of passed validation*:

![Passed validation](img/passed_validation.png)
