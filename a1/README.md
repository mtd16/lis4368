# LIS4368 - Advanced Web Application Development

## Michael DeMaria

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1 - 4)


#### README.md file should include the following items:

* Screenshots of running java Hello (#1 above)
* Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial)
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations).

> #### Git commands w/short descriptions:

1. git init- create a new local repository
2. git status- list files you have changed
3. git add- add one or more files to index
4. git commit- commit changes
5. git push- send changes to the master branch of your remote repos
6. git pull- fetch and merge changes on remote server to working
directory
7. git config- tell Git who you are

#### Assignment Screenshots:


*Screenshot of running AMMPS:*

![AMPPS](img/ampps_install.png)


*Screenshot of running java Hello:*

![Java Hello](img/java_install.png)


*Screenshot of running http://localhost:9999*

![Apache Tomcat http://localhost:9999](img/tomcat.png)


*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mtd16/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
