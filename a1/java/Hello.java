/*
 * First Java program to say Hello
 */
public class Hello {   // Save as "Hello.java"
   public static void main(String[] args) {
      System.out.println("Hello, world!");
   }
}