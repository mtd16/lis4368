# LIS4368 - Advanced Web Application Development

## Michael DeMaria

### Assignment 3 Requirements:

*Deliverables*:

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 record each table)
3. Provide Bitbucket read-only access to lis4368 repo.
    * docs folder: a3.mwb, and a3.sql
    * img folder: a3.png (export a3.mwb file as a3.png)
    * README.md (MUST display a3.png ERD)


#### README.md file should include the following items:

* Screenshot of Pet Store ERD that links to the image

#### Assignment Screenshots:


*Screenshot of A3 ERD:*

![A3 ERD](img/a3.png "ERD based upon A3 Requirements")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")
