# LIS4368 - Advanced Web Application Development

## Michael DeMaria

### Project 2 Requirements:

*Deliverables*:

1. Design a web application that uses CRUD functionality
2. Be able to display list of customers in application

#### README.md file should include the following items
1. Screenshot of valid user form
2. Screenshot of passed validation
3. Screenshot of modified data
4. Screenshot of delete warning
5. Screenshot of associated changes


#### Project Screenshots:

| Valid user form                  |  Sucessful Submission     |
|--------------------------            |------------------------|
| ![valid](images/valid_form.png) | ![Success](images/success.png) |
|                                      |                                      |

| Valid user form                  |  Sucessful Submission     |
|--------------------------            |------------------------|
| ![valid](images/valid_form.png) | ![Success](images/success.png) |
|                                      |                                      |

[Local Link to Lis4368 web app](http://localhost:9999/lis4368/ "Local Link")
