# LIS4368 - Advanced Web Application Development

## Mike DeMaria

### LIS4368 Requirements:

*Coursework Links:*

1. [A1 README.md](https://bitbucket.org/mtd16/lis4368/src/master/a1/)
	* Install JDK
	* Install Android Studio and create My First App and Contacts App
	* Provide screenshots of installations
	* Create Bitbucket repo
	* Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
	* Provide git command descriptions
2. [A2 README.md](https://bitbucket.org/mtd16/lis4368/src/master/a2/)
	* Complete Tomcat "Deploy A Web App" tutorial
	* Tutorial link: https://www.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html
	* Complete textbook questions (Chs. 5, 6)

3. [A3 README.md](https://bitbucket.org/mtd16/lis4368/src/master/a3/)
	* Design a Pet Store Entity Relationship Diagram (ERD) using MySQL Workbench
	* Provide screenshots of completed ERD
	* Provide corresponding SQL script
4. [P1 README.md](https://bitbucket.org/mtd16/lis4368/src/master/p1/)
	* Create server-side validation using Bootstrap
	* Add three slides to the homepage promoting your skills
	* Main/splash page should include three original slides
4. [A4 README.md](https://bitbucket.org/mtd16/lis4368/src/master/a4/)
	* Familiarize with MVC (Model View Controller) framework for internet web applications
	* Use Java Servlet to implement client side validation
	* Include screenshots of failed and passed validation examples
5. [A5 README.md](https://bitbucket.org/mtd16/lis4368/src/master/a5/)
	* Add database functionality to Assignment 4
	* Complete textbook chapter questions
6. [P2 README.md](https://bitbucket.org/mtd16/lis4368/src/master/p2/)
	* Design a Web Application that uses CRUD functionality
	* Be able to Correctly display list of data
