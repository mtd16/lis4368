# LIS4368 - Advanced Web Application Development

## Michael DeMaria

### Assignment 4 Requirements:

#### Model View Controller (MVC)is a framework that divides an application's implementation into three discrete component roles: models, views, and controllers.

#### Moreover, in computer science and information technology, separation of concerns (SoC) is a design principle for separating a computer program into distinct sections(or layers), with each section addressing a separate concern: e.g., presentation layer, business logic layer, data access layer, and persistence layer.

*Deliverables*:

1. Failed client-side validation
2. Passed client-side validation

#### Assignment Screenshots:

| Failed Validation                    |  Passed Validation     |
|--------------------------            |------------------------|
| ![Failed](img/failed_validation.png) | ![Passed](img/passed_validation.png) |
|                                      |                                      |

[Local Link to Lis4368 web app](http://localhost:9999/lis4368/ "Local Link")
