import java.util.Scanner;

public class Ascii {

    public static void main(String [] args) {
        Scanner sc = new Scanner(System.in);
        char letter = 'A';
        int num;
        boolean isValidNum = false;

        System.out.println("Printing characters A-Z as ASCII values:");

        while (letter <= 'Z')
        {
            num = letter;
            System.out.println("Character " + letter + " has ascii value " + num);
            letter++;
        }

        System.out.println("\nPrinting ASCII values 48-122 as characters:");

        num = 48;
        while (num <= 122)
        {
            System.out.println("ASCII value " + num + " has character value " + ((char)num));
            num++;
        }

        System.out.println("\nAllowing user ASCII value: ");

        while (isValidNum == false)
        {
            System.out.print("\nPlease enter ASCII value (32 - 127): ");
            if (sc.hasNextInt())
            {
                num = sc.nextInt();
                isValidNum = true;
            }

            else
            {
                System.out.print("Invalid integer--ASCII value must be a number.\n");
            }
            sc.nextLine();

            if (isValidNum == true && num < 32 || num > 127)
            {
                System.out.println("ASCII value must be >= 32 and <= 127.\n");
                isValidNum = false;
            }

            if (isValidNum == true)
            {
                System.out.println();
                System.out.printf("ASCII value %d has character value %c\n", num, ((char)num));
            }
        }
    }
}
