import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriteReadCountWords {
    public static void main(String args[]) {
        System.out.println("Program captures user input, writes to and reads from same file, and counts number of words in file.");
        System.out.println("Hint: use hasNext() method to read number of words (tokens).");
        System.out.println("");

        int wordCount = 1;
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter text: ");
        String text = input.nextLine();

        String filename = "filecountwords.txt";

        try {
            FileWriter writer = new FileWriter(filename, true);
            writer.write(text);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.printf("Saved to file " + '"' + filename + '"');
        System.out.println("");

        for (int i = 0; i < text.length(); ++i) {
            if (text.charAt(i) == ' ')
                wordCount++;
        }
        /*
        while (input.hasNextLine()) {
            text = input.nextLine();
            wordCount+= text.split("\\s+").length;
        }
        */

        System.out.println("Number of words: " + wordCount);

    }
}
