import java.util.Scanner;
public class CountCharacters
{
   public static void main(String args[])
   {
    //requirements
    System.out.println("Program counts number and types of characters: that is, letters, space, numbers, and other characters. ");
    System.out.println("Hint: You may find the following methods helpful: isLetter(), isDigit(), isSpaceChar(). ");
    System.out.println("Additionally, you could add the functionality of testing for upper vs lower case letters. ;)");
    System.out.println("");
    Scanner input = new Scanner(System.in);

    System.out.print("Please enter string: ");
    String str = input.nextLine();
    count(str);
   }
    public static void count(String str){
		char[] ch = str.toCharArray();
		int letter = 0;
		int space = 0;
		int num = 0;
		int other = 0;
		for(int i = 0; i < str.length(); i++){
			if(Character.isLetter(ch[i])){
				letter ++ ;
			}
			else if(Character.isDigit(ch[i])){
				num ++ ;
			}
			else if(Character.isSpaceChar(ch[i])){
				space ++ ;
			}
			else{
				other ++;
			}
		}
    
    //output
    System.out.println("Your string: " + '"' + str + '"' +  " has the following number and types of characters: ");
    System.out.println("letter(s): " + letter);
    System.out.println("space(s): " + space);
    System.out.println("number(s): " + num);
    System.out.println("other character(s): " + other);



   }
}
