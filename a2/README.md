# LIS4368 - Advanced Web Application Development

## Michael DeMaria

### Assignment 2 Requirements:

*Two Parts:*

1. Complete Tomcat "How to Get Started with Java Servlet Programming"
2. Textbook questions (Chs. 5, 6)


#### README.md file should include the following items:

* Directory Listing
* Home Page
* Bookshop Query
* Alternate Home Page
* Query Results with MySQL Java Connector

#### Assignment Screenshots:


*Directory Listing: http://localhost:9999/hello*

![](img/directory_listing.png)


*Hello Home: http://localhost:9999/hello/index.html*

![](img/home.png)


*Bookshop Query: http://localhost:9999/hello/querybook.html*

![](img/querybook.png)

*Hello world, again: http://localhost:9999/hello/sayhi*

![](img/say_hi.png)

*Bookshop Query Check: http://localhost:9999/hello/querybook.html*

![](img/querybook_check.png)

*Query Results: http://localhost:9999/hello/query?author=Tan+Ah+Teck*

![](img/query.png)
