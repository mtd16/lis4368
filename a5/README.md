# LIS4368 - Advanced Web Application Development

## Michael DeMaria

### Assignment 5 Requirements:

#### Along with the MVC framework—that is, a framework that divides an application's implementation into three discrete component roles: models, views, and controllers(see Assignment4 requirements)—programmers and web developers must be able to design and deploy data-driven applications. When doing so, the acronym CRUDis used, which generally means create, read, update and delete — sometimes, including SCRUD with an "S" for Search). These are the four basic functions of persistent storage.


*Deliverables*:

1. Valid user entry
2. Passed client-side validation
3. Associated MySQL Database Entry

#### Assignment Screenshots:

Valid Entry
![Valid](img/valid_user_entry.png)

Passed Validation
![Passed](img/passed_validation.png)

DB Entry
![Database Entry](img/db_entry.png)

[Local Link to Lis4368 web app](http://localhost:9999/lis4368/ "Local Link")
